import sqlite3
import time

from openpyxl import Workbook


today = time.strftime('%d%m%y')
wbook = Workbook()
wsheet = wbook.active
wsheet.title = f'latvpatent {today}'


wsheet.append(
    [
        'Date',
        'Link',
        'Reg Numurs',
        'Short Name',
        'Official Name',
        'Search Term',
        'Results'
    ]
)

CONN = sqlite3.connect('latvpatent.db')
LOAD_CURS = CONN.cursor()

LOAD_CURS.execute(
    """
        SELECT
            f.date,
            f.link ,
            f.inn,
            f.short_name,
            f.long_name ,
            res.searched ,
            res.results
        FROM firmas f
        LEFT JOIN lrpv res ON f.inn = res.inn;
    """
)
for row in LOAD_CURS.fetchall():
    wsheet.append(row)

wbook.save(f'latvpatent_{today}.xlsx')
