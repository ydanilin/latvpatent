.header on
.excel
SELECT
    f.date,
    f.link ,
    f.inn,
    f.short_name,
    f.long_name ,
    res.searched ,
    res.results
FROM firmas f
LEFT JOIN lrpv res ON f.inn = res.inn;
