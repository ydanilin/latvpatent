import os
import sys
import rollbar
from socket import gethostname
from time import sleep, strftime, gmtime
from psycopg2 import connect
from psycopg2.extras import DictCursor
import json
from functools import reduce, partial
from urllib.parse import urlencode
from requests import post
from tqdm import tqdm


URL_0 = "https://euipo.europa.eu/eSearch/#basic"
URL_1 = "https://euipo.europa.eu/copla/ctmsearch/json"
URL_2 = "https://euipo.europa.eu/copla/rcdsearch/json"
URL_3 = "https://euipo.europa.eu/copla/applicantsearch/json"
URL_4 = "https://euipo.europa.eu/copla/represearch/json"


ENV = os.environ.get("DB_SAME_HOST")
HOST = "134.209.82.166" if not ENV else "localhost"

rollbar.init('42b4b9fe073a43daae5eda9a80261762', 'production')

CONN = connect(
    host=HOST,
    user="latvpatent",
    password="shalom",
    # dbname="latvpatent",
    cursor_factory=DictCursor
)
LOAD_CURS = CONN.cursor()
SAVE_CURS = CONN.cursor()
ANTIBAN_DELAY = 5
COMMIT_INTERVAL = 5


def make_payload_dict(search, version):
    crit_1 = {
        1: "ApplicationNumber",
        2: "DesignIdentifier",
        3: "ApplicantIdentifier",
        4: "RepresentativeIdentifier",
    }
    crit_2 = {
        1: "MarkVerbalElementText",
        2: "VerbalElementText",
        3: "ApplicantName",
        4: "RepresentativeName",
    }

    output = {
        "start":  "0",
        "rows":  "100",
        "searchMode":  "basic",
        "criterion_1":  crit_1[version],
        "term_1":  search,
        "operator_1":  "OR",
        "condition_1":  "CONTAINS",
        "criterion_2":  crit_2[version],
        "term_2":  search,
        "operator_2":  "OR",
        "condition_2":  "CONTAINS",
        "sortField":  crit_1[version],
        "sortOrder":  "asc"
    }
    if version == 1:
        output.update(
            {
                "criterion_3":  "OppositionIdentifier",
                "term_3":  search,
                "operator_3":  "OR",
                "condition_3":  "CONTAINS",
            }
        )
    return output


def get_from_record_func(stopwords, record):
    short_name = record["short_name"]
    if short_name:
        return short_name.replace('"', '')
    filtered = reduce(
        lambda acc, x: acc.replace(x, ""),
        stopwords,
        record["long_name"]
    )
    return filtered.strip().replace('"', '')


get_from_record = partial(
    get_from_record_func,
    ["SIA", "IK", "Sabiedrība ar ierobežotu atbildību"]
)


def parse_result(record, response):
    result_json = json.loads(response.content)
    assert len(result_json.values()) == 4, (
        "Splash POST did not return "
        f"4 items for inn {record['inn']}, search {record['short_name']}"
    )
    counts_list = map(lambda x: json.loads(x)["total"], result_json.values())
    entries = reduce(lambda acc, x: acc + x, counts_list, 0)
    return entries


def get_startup_cookies(splash_endpoint, start_address):
    start_script = \
        """ function main(splash)
                assert(splash:go(splash.args.url))
                assert(splash:wait(1))
                return {
                    cookies = splash:get_cookies(),
                }
            end"""
    resp = post(
        splash_endpoint,
        json={
            'lua_source': start_script,
            'url': start_address,
        }
    )
    cook = json.loads(resp.content)
    assert len(cook.get("cookies", [])) > 0, "We did not get cookies!"
    return cook["cookies"]


def fetch_item(splash_endpoint, cookies, record):
    json_script = \
        """ treat = require("treat")

            function main(splash)
                splash:init_cookies(splash.args.cookies)
                result = {}
                for i = 1, #splash.args.requests do
                    details = splash.args.requests[i]
                    url = details["url"]
                    payload = treat.as_string(details["payload"])
                    resp = splash:http_post{url=url, body=payload}
                    s, h = treat.as_string(resp.body)
                    result[#result+1] = s
                end
                return result
            end"""
    search = get_from_record(record)
    if not search:
        return
    response = post(
        splash_endpoint,
        json={
            'lua_source': json_script,
            'cookies': cookies,  # Python lists of dict converted to Lua table
            'requests': [
                {"url": URL_1,
                 "payload": urlencode(make_payload_dict(search, 1))},
                {"url": URL_2,
                 "payload": urlencode(make_payload_dict(search, 2))},
                {"url": URL_3,
                 "payload": urlencode(make_payload_dict(search, 3))},
                {"url": URL_4,
                 "payload": urlencode(make_payload_dict(search, 4))}
            ]
        }
    )
    result = parse_result(record, response)

    # save to db
    insert = {
        "inn": record["inn"],
        "searched": search,
        "results": result
    }
    SAVE_CURS.execute(
        """INSERT INTO euipo (inn,
                             searched,
                             results)
            VALUES (
                %(inn)s,
                %(searched)s,
                %(results)s
            );""",
        insert
    )


def drive(splash_endpoint, amount=10):
    LOAD_CURS.execute(
        """
        SELECT count(*) FROM (
            SELECT * FROM firmas
            LEFT JOIN euipo ON
                firmas.inn = euipo.inn
            WHERE euipo.inn IS NULL
        ) AS t;
        """
    )
    total = LOAD_CURS.fetchone()[0]

    LOAD_CURS.execute(
        """
        SELECT f.* FROM firmas f
        LEFT JOIN euipo e ON
            f.inn = e.inn
        WHERE e.inn IS NULL;
        """
    )

    tm = strftime("%d/%m/%y %H:%M", gmtime())
    preamble = f"From {gethostname()} {tm} UTC, was {total} unscraped"
    print("Welcome to Europatents mekle!")
    print(preamble)
    progress_bar = tqdm(total=amount, unit='firma')
    try:
        cookies = get_startup_cookies(splash_endpoint, URL_0)
        for hit in range(amount):
            record = LOAD_CURS.fetchone()
            if not record:
                break
            fetch_item(splash_endpoint, cookies, record)
            if hit % COMMIT_INTERVAL == 0:
                CONN.commit()
            progress_bar.update(1)
            sleep(ANTIBAN_DELAY)
    except json.decoder.JSONDecodeError:
        outcome = (
            f"Banned after {progress_bar.last_print_n} items "
            f"(requested {amount})"
        )
        severity = "info"
    except AssertionError as err:
        if "not get cookies" in err.__str__():
            outcome = "Did not get initial cookies"
        else:
            outcome = f"Data error - {err.__str__()}"
        severity = "error"
    else:
        outcome = f"Full scrap of {amount} items!"
        severity = "info"
    finally:
        progress_bar.close()
        CONN.commit()
        CONN.close()
        print(outcome)
        msg = f"{preamble}: {outcome}"
        rollbar.report_message(msg, severity)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print(
            ("Need to supply max amount of records to parse\n"
             "  example: python euipo.py 100")
        )
        sys.exit(-1)
    amount = int(sys.argv[1])
    splash_endpoint_dev = "http://188.120.248.16:8050/execute"
    splash_endpoint_prod = "http://localhost:8050/execute"
    env = os.environ.get("EU_DEVELOP")
    endpoint = splash_endpoint_prod if not env else splash_endpoint_dev
    drive(endpoint, amount)
