CREATE TABLE IF NOT EXISTS
    firmas (
        batch INTEGER, 
        date TEXT, 
        link TEXT, 
        inn TEXT UNIQUE, 
        short_name TEXT, 
        long_name TEXT, 
        type TEXT
    );
CREATE TABLE IF NOT EXISTS
    lrpv (
        inn TEXT UNIQUE, 
        searched TEXT,
        results INTEGER        
    );
