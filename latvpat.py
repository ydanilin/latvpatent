import sys
import time
import sqlite3
from requests import Session
from utils.dbms import save_listing_and_count, get_db_inns
from utils.requestdetails import (get_listing_payload, get_item_payload,
                                  next_batch_number)
from utils.parse import parse_listing, parse_result, get_from_record
from utils.dateops import is_below_date


LIST_URL = "https://www.vestnesis.lv/op_ajax/komercregistra-zinas"
FIRMA_URL = "http://databases.lrpv.gov.lv/databases/lv/trademark/search"
ANTIBAN_DELAY = 0.5


CONN = sqlite3.connect('latvpatent.db')
LOAD_CURS = CONN.cursor()
SAVE_CURS = CONN.cursor()


def restart_line():
    sys.stdout.write("\r")
    sys.stdout.flush()


def drive(iterations=0):
    print("Welcome to Latvijas disappeared firmas!")

    stop_date = input("Please input the date for search (DD.MM.YYYY) ")
    # stop_date = '01.12.2019'
    list_ses = Session()
    firma_ses = Session()
    batch_number = next_batch_number()

    went_below_date = False

    print(f"We will scrape back until date: {stop_date}\n")

    while True:
        iter, batch = next(batch_number)
        if iterations and iter + 1 > iterations:
            break
        listing_resp = list_ses.post(LIST_URL, data=get_listing_payload(batch))
        listing_recs = parse_listing(listing_resp, batch)
        print(f"processing batch starting from {batch}")
        for i, record in enumerate(listing_recs):
            inns_seen = get_db_inns(LOAD_CURS)
            sys.stdout.write(f"  {i + 1}")
            sys.stdout.flush()
            restart_line()
            went_below_date = is_below_date(record["date"], stop_date)
            if record["inn"] in inns_seen or went_below_date:
                CONN.commit()
                continue
            search = get_from_record(record)
            if not search:
                continue
            resp = firma_ses.get(FIRMA_URL, params=get_item_payload(search))
            count = parse_result(resp, record)
            # import pudb; pudb.set_trace()
            save_listing_and_count(SAVE_CURS, record, search, count)
            time.sleep(ANTIBAN_DELAY)
        print(
            f"... earliest date from this batch: {listing_recs[-1]['date']}\n"
        )
        CONN.commit()
        if went_below_date:
            break


if __name__ == "__main__":
    try:
        drive()
    finally:
        CONN.commit()
        pass
    CONN.close()
