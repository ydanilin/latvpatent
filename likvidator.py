import sys
import time
import json
from psycopg2 import connect
from psycopg2.extras import DictCursor
from requests import Session
from utils.dbms import get_db_state, save_listing_and_count, get_db_inns
from utils.requestdetails import (get_listing_payload2, get_item_payload,
                                  next_batch_number)
from utils.parse import parse_listing, parse_result, get_from_record, parse_kri, parse_listing2
from utils.dateops import is_below_date

# browser_url = https://www.vestnesis.lv/oficialie-pazinojumi/komercregistra-zinas/apaksgrupa-KRI
LIST_URL = "https://www.vestnesis.lv/op_input/komercregistra-zinas"
DETAIL_URI = "https://www.vestnesis.lv/op_full/show_full_text"
FIRMA_URL = "http://databases.lrpv.gov.lv/databases/lv/trademark/search"
ANTIBAN_DELAY = 0.5


CONN = connect(
    host="134.209.82.166",
    user="latvpatent",
    password="shalom",
    # dbname="latvpatent",
    dbname="likvidator",
    cursor_factory=DictCursor
)
LOAD_CURS = CONN.cursor()
SAVE_CURS = CONN.cursor()


def restart_line():
    sys.stdout.write("\r")
    sys.stdout.flush()


def drive(iterations=0):
    print("Welcome to Latvijas disappeared firmas!")

    # stop_date, inns_seen = get_db_state(LOAD_CURS)
    stop_date = "01.02.2019"
    list_ses = Session()
    firma_ses = Session()
    batch_number = next_batch_number()

    went_below_date = False

    print(f"We will scrape back until date: {stop_date}\n")

    while True:
        iter, batch = next(batch_number)
        if iterations and iter + 1 > iterations:
            break
        listing_resp = list_ses.post(
            LIST_URL, data=get_listing_payload2(batch)
        )
        list_kris = parse_kri(listing_resp)
        details_resp = list_ses.post(
            DETAIL_URI, data={'op': json.dumps(list_kris)}
        )
        listing_recs = parse_listing2(details_resp, list_kris, batch)
        print(f"processing batch starting from {batch}")
        for i, record in enumerate(listing_recs):
            inns_seen = get_db_inns(LOAD_CURS)
            sys.stdout.write(f"  {i + 1}")
            sys.stdout.flush()
            restart_line()
            went_below_date = is_below_date(record["date"], stop_date)
            if record["inn"] in inns_seen or went_below_date:
                CONN.commit()
                continue
            search = get_from_record(record)
            if not search:
                continue
            resp = firma_ses.get(FIRMA_URL, params=get_item_payload(search))
            count = parse_result(resp, record)
            save_listing_and_count(SAVE_CURS, record, search, count)
            CONN.commit()
            time.sleep(ANTIBAN_DELAY)
        print(
            f"... earliest date from this batch: {listing_recs[-1]['date']}\n"
        )
        CONN.commit()
        if went_below_date:
            break


if __name__ == "__main__":
    try:
        drive(20)
    finally:
        CONN.commit()
        pass
    CONN.close()
