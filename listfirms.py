import re
import sqlite3
from tqdm import tqdm
from datetime import datetime
from functools import partial
from requests import Session
from bs4 import BeautifulSoup


# URL = "https://www.vestnesis.lv/oficialie-pazinojumi/komercregistra-zinas/apaksgrupa-KRL"
URL = "https://www.vestnesis.lv/op_ajax/komercregistra-zinas"
QUANT = 50
CONN = conn = sqlite3.connect('latvpatent.db')
CURS = CONN.cursor()


def extract_attr_func(selectors, item):
    for key, selector in selectors:
        result = selector.search(item)
        if result:
            return key, result.group(1)
    return None


extract_attrs = partial(
    extract_attr_func,
    (
        ("inn", re.compile(r"Reģistrācijas numurs\:\s+(\d+)")),
        ("long_name", re.compile(r"Firma\:\s+(.*)\.\.\.")),
    )
)


def extract_shorts_func(selector, rules, item):
    short_match = selector.search(item)
    short_name = short_match.group(1) if short_match else ""
    type = item.replace(short_name, "").replace('"', '').strip() if short_name else ""
    return (("short_name", short_name),
           ("type", rules.get(type, type)))

extract_shorts = partial(
    extract_shorts_func,
    re.compile(r"\"(.*)\""),
    {"Sabiedrība ar ierobežotu atbildību": "SIA"}
)


def get_payload(batch):
    return {
        "offset": batch,
        "show": "undefined",
        "vairak": "true",
        "sub": "KRL",
        "load_more_count": "true",
        "sub_grupa": "KRL",
    }

def get_container():
    return {
        "batch": "",
        "date": "",
        "inn": "",
        "link": "",
        "short_name": "",
        "long_name": "",
    }


def parse_listing(response, batch_no):
    output = []
    soup = BeautifulSoup(response.text, features="lxml")
    entries = soup.find_all("a", class_="article")
    for entry in entries:
        link = entry["href"]
        dt = entry.find("p", class_="docDay")
        date = dt.text.strip() if dt else ""
        text_gen = entry.find("p", class_="articleText").stripped_strings
        firma_details = filter(lambda x: x, map(extract_attrs, text_gen))
        record = get_container()
        record.update(firma_details)
        record.update(dict(batch=batch_no, link=link, date=date))
        record.update(extract_shorts(record["long_name"]))
        # stupidity
        if record["date"] == "Šodien":
            record["date"] = datetime.today().strftime("%d.%m.%Y")
        else:
            record["date"] = record["date"].rstrip(".")
        output.append(record)
    return output


def fetch_batch(session, batch_no):
    resp = session.post(URL, data=get_payload(batch_no))
    records = parse_listing(resp, batch_no)
    # print(record)
    CURS.executemany(
        """INSERT INTO firmas (batch,
                                date,
                                link,
                                inn,
                                short_name,
                                long_name,
                                type)
            VALUES (:batch,
                    :date,
                    :link,
                    :inn,
                    :short_name,
                    :long_name,
                    :type);""",
        records
    )
    CONN.commit()


def drive(iterations=1):
    batch_completed = CURS.execute(
        """SELECT MAX(batch) FROM firmas"""
    ).fetchone()[0]

    print("Welcome to Latvijas disappeared firmas!")
    print(f"Last completed batch # was {batch_completed}, will scrape {iterations} batches")

    progress_bar = tqdm(total=iterations, unit='batch')
    S = Session()
    
    for i in range(iterations):
        iter = i + 1
        next_batch = batch_completed + iter*QUANT
        # time.sleep(2)
        fetch_batch(S, next_batch)
        progress_bar.update(1)

    progress_bar.close()
    CONN.close()


if __name__ == "__main__":
    drive(1)
