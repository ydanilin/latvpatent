import time
import sqlite3
from functools import reduce, partial
from requests import Session
from bs4 import BeautifulSoup
from tqdm import tqdm


URL = "http://databases.lrpv.gov.lv/databases/lv/trademark/search"
ANTIBAN_DELAY = 0.5
CONN = conn = sqlite3.connect('latvpatent.db')
CONN.row_factory = sqlite3.Row
LOAD_CURS = CONN.cursor()
SAVE_CURS = CONN.cursor()
COMMIT_INTERVAL = 5


def make_query_dict(search):
    return {
        "regnroperation": "BEGINS",
        "regnr": "",
        "appnrboolop": "AND",
        "appnroperation": "BEGINS",
        "appnr": "",
        "verbboolop": "AND",
        "verboperation": "CONTAINS",
        "verb": search,
        "applicantboolop": "OR",
        "applicantoperation": "CONTAINS",
        "applicant": search,
        "representativeboolop": "AND",
        "representativeoperation": "CONTAINS",
        "representative": "",
        "nicaboolop": "AND",
        "nica": "",
        "viennaboolop": "AND",
        "vienna": "",
        "termsboolop": "AND",
        "terms": "",
        "typeboolop": "AND",
        "type": "",
        "groupboolop": "AND",
        "group": "1",
        "orderby": "applicationnumberorderasc",
        "pagesize": "10",
        "showbasket": "false",
    }


def get_from_record_func(stopwords, record):
    short_name = record["short_name"]
    if short_name:
        return short_name.replace('"', '')
    filtered = reduce(
        lambda acc, x: acc.replace(x, ""),
        stopwords,
        record["long_name"]
    )
    return filtered.strip().replace('"', '')


get_from_record = partial(
    get_from_record_func,
    ["SIA", "IK", "Sabiedrība ar ierobežotu atbildību"]
)


def parse_result(response, record):
    name = record["short_name"]
    soup = BeautifulSoup(response.text, features="lxml")
    result_div = soup.find("div", class_="results-count")
    if not result_div:
        return 100500
    texts = list(result_div.stripped_strings)
    assert len(texts) == 2, f"parse_result error for {name}: list len must be 2"
    assert "Atrasto ierakstu skaits" in texts[0], f"parse_result error for {name}: not valid results-count class"
    return int(texts[1])


def fetch_item(session, record):
    search = get_from_record(record)
    if not search:
        return
    resp = session.get(URL, params=make_query_dict(search))
    result = parse_result(resp, record)
    # save to db
    insert = {
        "inn": record["inn"],
        "searched": search,
        "results": result
    }
    SAVE_CURS.execute(
        """INSERT INTO lrpv (inn,
                             searched,
                             results)
            VALUES (:inn,
                    :searched,
                    :results);""",
        insert
    )


def drive(amount=10):
    total = LOAD_CURS.execute(
        """
        SELECT count() FROM (
            SELECT * FROM firmas
            LEFT JOIN lrpv ON
                firmas.inn = lrpv.inn
            WHERE lrpv.inn IS NULL
        );
        """
    ).fetchone()[0]

    print("Welcome to Latvijas patentu mekle!")
    print(f"Total items remain {total}, this run will scrape {amount}.")

    to_process = LOAD_CURS.execute(
        """
        SELECT * FROM firmas
        LEFT JOIN lrpv ON
            firmas.inn = lrpv.inn
        WHERE lrpv.inn IS NULL;
        """
    )

    progress_bar = tqdm(total=amount, unit='firma')
    S = Session()
    for hit in range(amount):
        record = to_process.fetchone()
        if not record:
            break
        fetch_item(S, record)
        if hit % COMMIT_INTERVAL == 0:
            CONN.commit()
        progress_bar.update(1)
        time.sleep(ANTIBAN_DELAY)

    progress_bar.close()
    CONN.commit()
    CONN.close()


if __name__ == "__main__":
    drive(5)
