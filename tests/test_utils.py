import unittest
import sys
sys.path.append("..")
from lrpv import get_from_record
from euipo import make_payload_dict


class TestUtils(unittest.TestCase):
    def test_get_from_record(self):
        records = [
            {"short_name": "",
            "long_name": "CNL GLOBAL Sabiedrība ar ierobežotu atbildību",
            "expected": "CNL GLOBAL"},
            {"short_name": "",
            "long_name": "BALTIC MOBILE SIA",
            "expected": "BALTIC MOBILE"},
            {"short_name": "",
            "long_name": "IK NI.8.TA",
            "expected": "NI.8.TA"},
            {"short_name": "",
            "long_name": "IK KBK",
            "expected": "KBK"},
            {"short_name": "",
            "long_name": "SIA LUUM",
            "expected": "LUUM"},
        ]

        for record in records:
            result = get_from_record(record)
            self.assertEqual(result, record["expected"])

    def test_make_payload_dict_1(self):
        expected = {
            "start": "0",
            "rows":  "100",
            "searchMode":  "basic",
            "criterion_1":  "ApplicationNumber",
            "term_1":  "daiss",
            "operator_1":  "OR",
            "condition_1":  "CONTAINS",
            "criterion_2":  "MarkVerbalElementText",
            "term_2":  "daiss",
            "operator_2":  "OR",
            "condition_2":  "CONTAINS",
            "criterion_3":  "OppositionIdentifier",
            "term_3":  "daiss",
            "operator_3":  "OR",
            "condition_3":  "CONTAINS",
            "sortField":  "ApplicationNumber",
            "sortOrder":  "asc",
        }
        result = make_payload_dict("daiss", 1)
        self.assertDictEqual(result, expected)

    def test_make_payload_dict_2(self):
        expected = {
            "start":  "0",
            "rows":  "100",
            "searchMode":  "basic",
            "criterion_1":  "DesignIdentifier",
            "term_1":  "daiss",
            "operator_1":  "OR",
            "condition_1":  "CONTAINS",
            "criterion_2":  "VerbalElementText",
            "term_2":  "daiss",
            "operator_2":  "OR",
            "condition_2":  "CONTAINS",
            "sortField":  "DesignIdentifier",
            "sortOrder":  "asc"
        }
        result = make_payload_dict("daiss", 2)
        self.assertDictEqual(result, expected)

    def test_make_payload_dict_3(self):
        expected = {
            "start":  "0",
            "rows":  "100",
            "searchMode":  "basic",
            "criterion_1":  "ApplicantIdentifier",
            "term_1":  "daiss",
            "operator_1":  "OR",
            "condition_1":  "CONTAINS",
            "criterion_2":  "ApplicantName",
            "term_2":  "daiss",
            "operator_2":  "OR",
            "condition_2":  "CONTAINS",
            "sortField":  "ApplicantIdentifier",
            "sortOrder":  "asc"
        }
        result = make_payload_dict("daiss", 3)
        self.assertDictEqual(result, expected)

    def test_make_payload_dict_4(self):
        expected = {
            "start":  "0",
            "rows":  "100",
            "searchMode":  "basic",
            "criterion_1":  "RepresentativeIdentifier",
            "term_1":  "daiss",
            "operator_1":  "OR",
            "condition_1":  "CONTAINS",
            "criterion_2":  "RepresentativeName",
            "term_2":  "daiss",
            "operator_2":  "OR",
            "condition_2":  "CONTAINS",
            "sortField":  "RepresentativeIdentifier",
            "sortOrder":  "asc"
        }
        result = make_payload_dict("daiss", 4)
        self.assertDictEqual(result, expected)
