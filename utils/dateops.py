from typing import List, Tuple
from datetime import datetime, timedelta, timezone
from calendar import timegm
from time import strptime, strftime, gmtime
from .weekends import weekends


TIME_FORMAT = "%d.%m.%Y"


def date_generator(from_seconds: int, downto_seconds: int) -> int:
    ret_seconds = from_seconds
    while ret_seconds >= downto_seconds:
        yield ret_seconds
        this_day = datetime.fromtimestamp(ret_seconds, timezone.utc)
        back_one_day = this_day - timedelta(days=1)
        ret_seconds = int(back_one_day.timestamp())


# https://realpython.com/python-type-checking/
def calculate_stop_date(list_of_dates: List[str]) -> Tuple[str, str]:
    saved_dates = set(
        map(lambda x: int(timegm(strptime(x, TIME_FORMAT))), list_of_dates)
    )
    minimum = min(saved_dates)
    maximum = max(saved_dates)
    calend = set(date_generator(maximum, minimum))
    days_no_records = calend.difference(saved_dates, weekends)
    last_date = strftime(TIME_FORMAT, gmtime(maximum))
    stop_date = (
        strftime(TIME_FORMAT, gmtime(min(days_no_records)))
        if days_no_records else last_date
    )
    return last_date, stop_date


def get_list_of_dates(from_date: str, downto_date: str) -> Tuple[str]:
    from_seconds = int(timegm(strptime(from_date, TIME_FORMAT)))
    downto_seconds = int(timegm(strptime(downto_date, TIME_FORMAT)))
    to_query = tuple(
        map(
            lambda x: strftime(TIME_FORMAT, gmtime(x)),
            date_generator(from_seconds, downto_seconds)
        )
    )
    return to_query


def is_below_date(date_to_check, stop_date):
    seconds_to_check = int(timegm(strptime(date_to_check, TIME_FORMAT)))
    stop_seconds = int(timegm(strptime(stop_date, TIME_FORMAT)))

    this_day = datetime.fromtimestamp(stop_seconds, timezone.utc)
    back_one_day = this_day - timedelta(days=1)
    stop = int(back_one_day.timestamp())

    if seconds_to_check < stop:
        return True
    else:
        return False
