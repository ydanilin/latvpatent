from typing import Tuple, Set, Dict
import operator
from functools import reduce
from .dateops import calculate_stop_date, get_list_of_dates


def get_firma_container():
    return {
        "batch": "",
        "date": "",
        "inn": "",
        "link": "",
        "short_name": "",
        "long_name": "",
    }


def get_db_state(cursor: object) -> Tuple[str, Set]:
    """
    Returns date of the earliest record (also analyzing gaps) +
    set of INNs dated earliest
    """
    cursor.execute("""SELECT date FROM firmas GROUP BY date;""")
    from_db = cursor.fetchall()
    dates_from_db = reduce(operator.iconcat, from_db, [])
    last_date, stop_date = calculate_stop_date(dates_from_db)
    to_query = get_list_of_dates(last_date, stop_date)

    cursor.execute("""SELECT inn FROM firmas WHERE date IN %s;""", (to_query,))
    from_db = reduce(operator.iconcat, cursor.fetchall(), [])
    inns_seen = set(from_db)

    return stop_date, inns_seen


def get_db_inns(cursor):
    cursor.execute("""SELECT inn FROM firmas;""")
    from_db = reduce(operator.iconcat, cursor.fetchall(), [])
    inns_seen = set(from_db)
    return inns_seen


def save_listing_and_count(
    cursor, listing: Dict, actual_search: str, count: int
) -> None:
    """ Warning! Does not commit! """
    cursor.execute(
        """INSERT INTO firmas (batch,
                                date,
                                link,
                                inn,
                                short_name,
                                long_name,
                                type)
            VALUES (:batch,
                    :date,
                    :link,
                    :inn,
                    :short_name,
                    :long_name,
                    :type);""",
        listing
    )

    insert = {
        "inn": listing["inn"],
        "searched": actual_search,
        "results": count
    }
    cursor.execute(
        """INSERT INTO lrpv (inn,
                             searched,
                             results)
            VALUES (:inn,
                    :searched,
                    :results);""",
        insert
    )
