from typing import Tuple
from functools import reduce


def match_regexes_to_string(
    horde_of_regexes: Tuple[Tuple[str, object]],
    text
) -> Tuple[str, str]:
    """ Applies a series of regexps to a text string.
        First regexp that matches causes return the
        extracted stuff.
        Otherwise returns None
    """
    for key, regexp in horde_of_regexes:
        result = regexp.search(text)
        if result:
            return key, result.group(1)
    return None


def extract_short_nametype(regexp_selector, how_to_abbrev, text):
    """
    Examples what it does:
    Sabiedrība ar ierobežotu atbildību "HUJS" ->
    (
        ('short_name', 'HUJS'),
        ('type', 'SIA')
    )
    Sabiedrība ar ierobežotu atbildību "Kempings "VECUPE"" ->
    (
        ('short_name', Kempings "VECUPE"),
        ('type', 'SIA')
    )
    """
    short_match = regexp_selector.search(text)
    short_name = short_match.group(1) if short_match else ""
    # next line does:
    # Sabiedrība ar ierobežotu atbildību "Kempings "VECUPE"" ->
    # Sabiedrība ar ierobežotu atbildību
    type = (
        text.replace(short_name, "").replace('"', '').strip()
        if short_name else ""
    )
    return (
        ("short_name", short_name),
        # how_to_abbrev is a dict {long type: short type}
        ("type", how_to_abbrev.get(type, type))
    )


def shortenize_from_db(stopwords, record):
    """
    Tries to create firma short name from db record
    if there is no short name in db for some reason
    """
    short_name = record["short_name"]
    if short_name:
        return short_name.replace('"', '')
    filtered = reduce(
        lambda acc, x: acc.replace(x, ""),
        stopwords,
        record["long_name"]
    )
    return filtered.strip().replace('"', '')
