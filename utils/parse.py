import re
from functools import partial
from datetime import datetime
from bs4 import BeautifulSoup
from .extract import (match_regexes_to_string, extract_short_nametype,
                      shortenize_from_db)
from .dbms import get_firma_container


# **************************************************
# listing parse functions
# **************************************************

def parse_kri(response):
    kris = []
    soup = BeautifulSoup(response.json()['html'], features="lxml")
    kri_tags = soup.findAll('p', class_="docDay docNr")
    for kri_tag in kri_tags:
        kri = kri_tag.text.strip().replace('OP ', '')
        kris.append(kri)
    return kris


extract_firma_info = partial(
    match_regexes_to_string,
    (
        ("inn", re.compile(r"Reģistrācijas numurs\:\s+(\d+)")),
        # ("long_name", re.compile(r"Firma\:\s+(.*)\.\.\.")),
        ("long_name", re.compile(r"Firma\:\s+(.*)\.{3}")),
    )
)


extract_shorts = partial(
    extract_short_nametype,
    re.compile(r"\"(.*)\""),
    {"Sabiedrība ar ierobežotu atbildību": "SIA"}
)


def parse_listing(response, batch_no):
    output = []
    soup = BeautifulSoup(response.text, features="lxml")
    entries = soup.find_all("a", class_="article")
    for entry in entries:
        link = entry["href"]
        dt = entry.find("p", class_="docDay")
        date = dt.text.strip() if dt else ""
        text_gen = entry.find("p", class_="articleText").stripped_strings
        firma_details = filter(lambda x: x, map(extract_firma_info, text_gen))
        record = get_firma_container()
        record.update(firma_details)
        record.update(dict(batch=batch_no, link=link, date=date))
        record.update(extract_shorts(record["long_name"]))
        # stupidity
        if record["date"] == "Šodien":
            record["date"] = datetime.today().strftime("%d.%m.%Y")
        else:
            record["date"] = record["date"].rstrip(".")
        output.append(record)
    return output


def parse_listing2(response, list_kris, batch_no):
    output = []
    # transform list of dicts to dict
    entries = {i['op']: i['html'] for i in response.json()}
    for kri in list_kris:
        soup = BeautifulSoup(entries[kri], features="lxml")
        link = soup.find('a', class_="pubTextLink")["href"].strip()
        dt = soup.find("p", class_="docDay")
        date = dt.text.strip() if dt else ""
        text_gen = soup.find("div", class_="articleText").stripped_strings
        firma_details = filter(lambda x: x, map(extract_firma_info, text_gen))
        record = get_firma_container()
        record.update(firma_details)
        record.update(dict(batch=batch_no, link=link, date=date))
        record.update(extract_shorts(record["long_name"]))
        # stupidity
        if record["date"] == "Šodien":
            record["date"] = datetime.today().strftime("%d.%m.%Y")
        else:
            record["date"] = record["date"].rstrip(".")
        output.append(record)
    return output


# **************************************************
# item parse functions
# **************************************************

get_from_record = partial(
    shortenize_from_db,
    ["SIA", "IK", "Sabiedrība ar ierobežotu atbildību"]
)


def parse_result(response, record):
    name = record["short_name"]
    soup = BeautifulSoup(response.text, features="lxml")
    result_div = soup.find("div", class_="results-count")
    if not result_div:
        return 100500
    texts = list(result_div.stripped_strings)
    assert len(texts) == 2, f"parse_result error for {name}: list len must be 2"
    assert "Atrasto ierakstu skaits" in texts[0], f"parse_result error for {name}: not valid results-count class"
    return int(texts[1])
