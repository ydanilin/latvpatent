
def get_listing_payload(batch):
    return {
        "offset": batch,
        "show": "undefined",
        "vairak": "true",
        "sub": "KRL",
        "load_more_count": "true",
        "sub_grupa": "KRL",
    }


def get_listing_payload2(batch):
    return {
        'fname': 'LIKVIDATORS',
        'offset': batch,
        'sub_grupa': 'KRI',
        'precise': 'true',
        'load_more_count': 'true',
    }


LISTING_QUANT = 50


def next_batch_number():
    iter = 0
    while True:
        yield iter, iter*LISTING_QUANT
        iter += 1


def get_item_payload(search):
    return {
        "regnroperation": "BEGINS",
        "regnr": "",
        "appnrboolop": "AND",
        "appnroperation": "BEGINS",
        "appnr": "",
        "verbboolop": "AND",
        "verboperation": "CONTAINS",
        "verb": "",
        "applicantboolop": "AND",
        "applicantoperation": "BEGINS",  # applicant - Sakas ar
        "applicant": search,
        "representativeboolop": "AND",
        "representativeoperation": "CONTAINS",
        "representative": "",
        "nicaboolop": "AND",
        "nica": "",
        "viennaboolop": "AND",
        "vienna": "",
        "termsboolop": "AND",
        "terms": "",
        "typeboolop": "AND",
        "type": "",
        "groupboolop": "AND",
        "group": "3",  # 3 = Speka esoshas registracijas
        "orderby": "applicationnumberorderasc",
        "pagesize": "10",
        "showbasket": "false",
    }
