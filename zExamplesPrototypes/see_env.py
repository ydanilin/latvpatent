import os
import sys

env = os.environ.get("EU_DEVELOP")
if env:
    print(f"EU_DEVELOP exsists and equals to {env}")
else:
    print("EU_DEVELOP is absent")
print(sys.argv)
if len(sys.argv) < 2:
    print(
        ("Need to supply max amount of records to parse\n"
         "  example: python euipo.py 100")
    )
    sys.exit(-1)
amount = int(sys.argv[1])
print(amount)
