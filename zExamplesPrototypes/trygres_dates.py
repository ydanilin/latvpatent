from psycopg2 import connect
from psycopg2.extras import DictCursor
import operator
from functools import reduce
from time import strptime, strftime, gmtime
from calendar import timegm
from datetime import datetime, timedelta, timezone
from weekends import weekends


def date_generator(from_seconds, downto_seconds):
    ret_seconds = from_seconds
    while ret_seconds >= downto_seconds:
        yield ret_seconds
        this_day = datetime.fromtimestamp(ret_seconds, timezone.utc)
        back_one_day = this_day - timedelta(days=1)
        ret_seconds = int(back_one_day.timestamp())


CONN = connect(
    host="134.209.82.166",
    user="latvpatent",
    password="shalom",
    dbname="latvpatent",
    cursor_factory=DictCursor
)

CURS = CONN.cursor()

TIME_FORMAT = "%d.%m.%Y"

CURS.execute("""SELECT date FROM firmas GROUP BY date;""")
from_db = CURS.fetchall()
res = reduce(operator.iconcat, from_db, [])
saved_dates = set(map(lambda x: int(timegm(strptime(x, TIME_FORMAT))), res))
minimum = min(saved_dates)
maximum = max(saved_dates)
leng = len(saved_dates)

print(
    (f"Records are from {strftime(TIME_FORMAT, gmtime(minimum))} ({minimum}) "
     f"to {strftime(TIME_FORMAT, gmtime(maximum))} ({maximum}), amount {leng}")
)

calend = set(date_generator(maximum, minimum))
minimum = min(calend)
maximum = max(calend)
leng = len(calend)

print(
    (f"Calendar is from {strftime(TIME_FORMAT, gmtime(minimum))} ({minimum}) "
     f"to {strftime(TIME_FORMAT, gmtime(maximum))} ({maximum}), amount {leng}")
)

absent = calend.difference(saved_dates, weekends)
absent = list(absent)
absent.sort(reverse=True)
# mini = min(absent)

print(
    list(map(lambda x: strftime(TIME_FORMAT, gmtime(x)), absent))
)

CURS.close()
CONN.close()
