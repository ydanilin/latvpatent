from psycopg2 import connect
from psycopg2.extras import DictCursor
import operator
from functools import reduce
import sys
sys.path.append("..")
from utils.dateops import calculate_stop_date, get_list_of_dates


CONN = connect(
    host="134.209.82.166",
    user="latvpatent",
    password="shalom",
    dbname="latvpatent",
    cursor_factory=DictCursor
)

LOAD_CURS = CONN.cursor()

LOAD_CURS.execute("""SELECT date FROM firmas GROUP BY date;""")
from_db = LOAD_CURS.fetchall()
dates_from_db = reduce(operator.iconcat, from_db, [])
last_date, stop_date = calculate_stop_date(dates_from_db)
to_query = get_list_of_dates(last_date, stop_date)

LOAD_CURS.execute("""SELECT inn FROM firmas WHERE date IN %s;""", (to_query,))
from_db = reduce(operator.iconcat, LOAD_CURS.fetchall(), [])
inns_seen = set(from_db)

print(from_db)

LOAD_CURS.close()
CONN.close()
