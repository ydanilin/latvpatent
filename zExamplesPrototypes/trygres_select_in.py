from psycopg2 import connect
from psycopg2.extras import DictCursor
from time import strptime, strftime, gmtime
from calendar import timegm
from datetime import datetime, timedelta, timezone


def date_generator(from_seconds, downto_seconds):
    ret_seconds = from_seconds
    while ret_seconds >= downto_seconds:
        yield ret_seconds
        this_day = datetime.fromtimestamp(ret_seconds, timezone.utc)
        back_one_day = this_day - timedelta(days=1)
        ret_seconds = int(back_one_day.timestamp())


CONN = connect(
    host="134.209.82.166",
    user="latvpatent",
    password="shalom",
    dbname="latvpatent",
    cursor_factory=DictCursor
)

CURS = CONN.cursor()

TIME_FORMAT = "%d.%m.%Y"

from_seconds = int(timegm(strptime('14.03.2019', TIME_FORMAT)))
downto_seconds = int(timegm(strptime('15.02.2019', TIME_FORMAT)))

to_query = tuple(
    map(
        lambda x: strftime(TIME_FORMAT, gmtime(x)),
        date_generator(from_seconds, downto_seconds)
    )
)

CURS.execute("""SELECT date FROM firmas WHERE date IN %s;""", (to_query,))
from_db = CURS.fetchall()

print(from_db)

CURS.close()
CONN.close()
