import json
from requests import post
from functools import reduce
from urllib.parse import urlencode
from euipo import make_payload_dict


URL_1 = "https://euipo.europa.eu/copla/ctmsearch/json"
URL_2 = "https://euipo.europa.eu/copla/rcdsearch/json"
URL_3 = "https://euipo.europa.eu/copla/applicantsearch/json"
URL_4 = "https://euipo.europa.eu/copla/represearch/json"


start_script = \
""" function main(splash)
        assert(splash:go(splash.args.url))
        assert(splash:wait(1))
        return {
            cookies = splash:get_cookies(),
        }
    end"""


json_script = \
""" treat = require("treat")

    function main(splash)
        splash:init_cookies(splash.args.cookies)
        result = {}
        for i = 1, #splash.args.requests do
            details = splash.args.requests[i]
            url = details["url"]
            payload = treat.as_string(details["payload"])
            resp = splash:http_post{url=url, body=payload}
            s, h = treat.as_string(resp.body)
            result[#result+1] = s
        end
        return result
    end"""


SPLASH_ENDPOINT = "http://188.120.248.16:8050/execute"


resp = post(
    SPLASH_ENDPOINT,
    json = {
        'lua_source': start_script,
        'url': 'https://euipo.europa.eu/eSearch/#basic'
    }
)

cook = json.loads(resp.content)
assert len(cook.get("cookies", [])) > 0, "We did not get cookies!"

resp1 = post(
    SPLASH_ENDPOINT,
    json = {
        'lua_source': json_script,
        'cookies': cook["cookies"],  # accept Python list directly and convert it to Lua table
        'requests': [
            {"url": URL_1, "payload": urlencode(make_payload_dict("daiss", 1))},
            {"url": URL_2, "payload": urlencode(make_payload_dict("daiss", 2))},
            {"url": URL_3, "payload": urlencode(make_payload_dict("daiss", 3))},
            {"url": URL_4, "payload": urlencode(make_payload_dict("daiss", 4))}
        ]
    }
)

# import pudb; pudb.set_trace()
result_json = json.loads(resp1.content)
assert len(result_json.values()) == 4, "Splash POST did not return 4 items"
counts_list = map(lambda x: json.loads(x)["total"], result_json.values())
entries = reduce(lambda acc, x: acc + x, counts_list, 0)
print(entries)
